#include <stdio.h>
#include <stdlib.h>

unsigned char checkprime(unsigned long num, int prime, int length, unsigned long *prime_array){
	for(int i = 0; i <= length; i++){
		if(prime_array[i] != 0){
			if(num % prime_array[i] == 0){
				return 0;
			}
		}
	}
	prime_array[prime] = num;
	return 1;
}

int main(int argc, char *argv[]){
	if(!argv[1]){
		fprintf(stderr, "Usage: %s <num of primes>\n", argv[0]);
		return 1;
	}
	
	unsigned long prime_array[atoi(argv[1])];
	prime_array[0] = 2;

	int j = 1;
	unsigned long a = 3;

	while(j <= atoi(argv[1])){
		if(checkprime(a, j, atoi(argv[1]), prime_array)){
			printf("%lu\n", a);
			j++;
		}
		a++;
	}
	
	return 0;
}
