all : clean primes
	rm -f *.o

primes : primes.o
	clang -o primes primes.o

primes.o :
	clang -c primes.o

clean :
	rm primes primes.o
